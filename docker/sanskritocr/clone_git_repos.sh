#!/usr/bin/env bash
version=$1
echo "Deploying Software Version ${version}"
declare repo_names=("apps/textract/textract-sanskritocr" "libs/vv-client-python" "libs/libtextract")
for repo in "${repo_names[@]}"; do
	giturl="https://gitlab.com/vedavaapi/${repo}.git"
	echo "${giturl}"
	git clone ${giturl}
	cd $(echo "${repo}" | sed 's#^.*/##;')
	git checkout ${version}
	cd -
done
