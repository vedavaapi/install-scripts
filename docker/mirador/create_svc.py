import argparse
import sys
import os
import time


default_app_name = 'mirador'
default_version = 'master'

def persist_env_vars(args):
    env_file_content = ''

    rebuild_ts = 0
    if os.path.exists(".env"):
        with open(".env") as f:
            lines = f.read().split('\n')
            for l in lines:
                if '=' not in l:
                    continue
                k, v = l.split('=')
                if k == 'rebuild_flag':
                    rebuild_ts = v
                    print("Retrieved old rebuild_ts {}".format(rebuild_ts))

                    break

    app_name = getattr(args, 'app_name', None)
    if app_name:
        env_file_content += "COMPOSE_PROJECT_NAME={}\n".format(app_name)

    env_file_content += "version={}\n".format(args.version)
    env_file_content += "port={}\n".format(args.port)
    env_file_content += "rebuild_flag={}\n".format(time.time() if args.rebuild else rebuild_ts)
    env_file_content += "mount_path={}\n".format(args.mount_path)
    env_file_content += "vv_proxy={}\n".format(args.vv_proxy)

    open('.env', 'wb').write(env_file_content.encode('utf-8'))


def invoke_docker_compose(args):
    os.system("docker-compose up -d --build")


def main(argv):
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '-p', help='port', dest='port', required=False, default='9501'
    )
    parser.add_argument(
        '-n', '--app_name', help='app_name',
        dest='app_name', default=default_app_name, required=False
    )
    parser.add_argument(
        '-m', '--mount_path', help='mount_path',
        dest='mount_path', default='', required=False
    )
    parser.add_argument(
        '-V', '--version', help='version/branch/commit',
        dest='version', default=default_version, required=False
    )
    parser.add_argument(
        '-l', '--rebuild', help='Force rebuild of Docker container',
        dest='rebuild', action="store_true", required=False
    )
    parser.add_argument(
        '--vv_proxy', help='docker proxy common network', dest='vv_proxy', required=False, default='vv_proxy'
    )

    args, unknown = parser.parse_known_args()
    args.mount_path = args.mount_path.lstrip('/').rstrip('/')

    persist_env_vars(args)
    invoke_docker_compose(args)

if __name__ == '__main__':
    main(sys.argv[:])
