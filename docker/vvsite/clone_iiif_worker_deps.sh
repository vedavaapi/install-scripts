version=$1
echo "Deploying Vedavaapi Software Version ${version}"
declare repo_names=("platform/core_services" "platform/vv_schemas" "platform/libvvobjstore" "platform/vedavaapi-importer" "platform/tasker" "platform/abstractfs" "libs/vv-client-python")
for repo in "${repo_names[@]}"; do
	giturl="https://gitlab.com/vedavaapi/${repo}.git"
	git clone "${giturl}"
	cd $(echo "${repo}" | sed 's#^.*/##;') || exit
	git checkout "${version}"
    git pull
	cd - || exit
done
