import argparse
import json
import re
import sys
import time
from shutil import copyfile
# noinspection PyCompatibility

import os

default_site_name = 'myvvsite'
default_vv_host_dir = '/opt/vedavaapi'
vv_version = 'master'

def generate_conf_data(args):
    if not os.path.exists('conf_data/creds/oauth'):
        os.makedirs('conf_data/creds/oauth')
    if args.google_client_creds_path:
        if not os.path.exists('conf_data/creds/oauth/google'):
            os.makedirs('conf_data/creds/oauth/google')
        try:
            copyfile(args.google_client_creds_path, 'conf_data/creds/oauth/google/default.json')
        except Exception as e:
            print('error in copying google creds file', e)

    org_config = {
        "db_type": "mongo",
        "db_host": "mongodb://mongo:27017",
        "db_prefix": "vv",
        "file_store_base_path": "vv"
    }

    orgs_config = {
        "default": org_config
    }

    with open('conf_data/orgs.json', 'wb') as orgs_file:
        orgs_file.write(json.dumps(orgs_config, indent=2, ensure_ascii=False).encode('utf-8'))


def _get_objstore_fiels_dir(args):
    if args.objstore_files_dir:
        return args.objstore_files_dir
    return os.path.join(args.vv_host_dir, 'objstore_files')

def setup_volume_dirs(args):
    paths = [
        os.path.join(args.vv_host_dir, t)
        for t in ('mongo', 'vv', 'logs')
    ]
    paths.append(_get_objstore_fiels_dir(args))
    for path in paths:
        if args.reset:
            print("Removing {}".format(path))
            os.system('rm -rf {}'.format(path))
        if not os.path.exists(path):
            os.system('mkdir -p {}'.format(path))


def persist_env_vars(args):
    env_file_content = ''

    rebuild_ts = 0
    if os.path.exists(".env"):
        with open(".env") as f:
            lines = f.read().split('\n')
            for l in lines:
                if '=' not in l:
                    continue
                k, v = l.split('=')
                if k == 'rebuild_flag':
                    rebuild_ts = v
                    print("Retrieved old rebuild_ts {}".format(rebuild_ts))
                    break

    vv_site_name = getattr(args, 'vv_site_name', None)
    if vv_site_name:
        env_file_content += "COMPOSE_PROJECT_NAME={}\n".format(vv_site_name)
    env_file_content += "services={}\n".format(' '.join(args.services.split(',')))
    env_file_content += "url_mount_path={}\n".format(args.url_mount_path.lstrip('/'))
    env_file_content += "vv_version={}\n".format(args.version)
    env_file_content += "vv_mongo_dir={}\n".format(os.path.join(args.vv_host_dir, 'mongo'))
    env_file_content += "vv_data_dir={}\n".format(os.path.join(args.vv_host_dir, 'vv'))
    env_file_content += "vv_log_dir={}\n".format(os.path.join(args.vv_host_dir, 'logs'))
    env_file_content += "objstore_files_dir={}\n".format(
        _get_objstore_fiels_dir(args))
    env_file_content += "vv_port={}\n".format(args.vv_port)
    env_file_content += "rebuild_flag={}\n".format(time.time() if args.rebuild else rebuild_ts)
    env_file_content += "vv_proxy={}\n".format(args.vv_proxy)

    open('.env', 'wb').write(env_file_content.encode('utf-8'))


def invoke_docker_compose():
    print(os.getcwd())
    return os.system("docker-compose up -d --build")


def main(argv):
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--gcreds', help='google credentials file path',
        dest='google_client_creds_path', default='vv_testsite_google_creds.json'
    )
    parser.add_argument(
        '-s', '--svc', help='comma-separated list of services to be launched', dest='services', required=False, default="schemas,accounts,objstore,iiif_image,iiif_presentation,importer,abstractfs,tasker"
    )
    parser.add_argument(
        '-m', help='url mount path', dest='url_mount_path', required=False, default=''
    )
    parser.add_argument(
        '-d', help='vedavaapi host dir prefix', dest='vv_host_dir', required=False, default=default_vv_host_dir
    )
    parser.add_argument(
        '-r', '--reset', help='reset previous service', action='store_true', dest='reset'
    )
    parser.add_argument(
        '-p', "--port", help='port', dest='vv_port', required=False, default='9010'
    )
    parser.add_argument(
        '-n', '--name', help='Vedavaapi site name',
        dest='vv_site_name', default=default_site_name, required=False
    )
    parser.add_argument(
        '-V', '--version', help='Vedavaapi Software Version tag',
        dest='version', default=vv_version, required=False
    )
    parser.add_argument(
        '-l', '--rebuild', help='force reload', dest='rebuild', required=False, action='store_true'
    )
    parser.add_argument(
        '--objstore-files-dir', help='Objstore files path',
        dest='objstore_files_dir', default=None, required=False
    )
    parser.add_argument(
        '--vv_proxy', help='docker proxy common network', dest='vv_proxy', required=False, default='vv_proxy'
    )

    args, unknown = parser.parse_known_args()

    print('1. generating configuration data')
    generate_conf_data(args)
    print("2. creating vedavaapi volumes")
    setup_volume_dirs(args)
    print("3. persisting docker-compose env vars")
    persist_env_vars(args)
    print("4. invoking docker-compose......")
    return invoke_docker_compose()


if __name__ == '__main__':
    sys.exit(main(sys.argv[:]))
