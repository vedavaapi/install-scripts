version=$1
echo "Deploying Vedavaapi Software Version ${version}"
declare repo_names=("platform/core_services" "platform/vedavaapi_api" "platform/vv_schemas" "platform/libvvobjstore" "platform/vv_objstore" "platform/iiif" "platform/vedavaapi-importer" "platform/abstractfs" "platform/tasker" "libs/vv-client-python" "libs/google_services_helper")
for repo in ${repo_names[@]}; do
	giturl="https://gitlab.com/vedavaapi/${repo}.git"
	git clone ${giturl}
	cd $(echo "${repo}" | sed 's#^.*/##;') || exit;
	git checkout ${version}
    git pull
	#echo "export PYTHONPATH=\"\${PYTHONPATH}:$(pwd)\"" >> ~/.bashrc
	cd -
done
