import argparse
import sys
import os
import time

default_app_name = 'vedavaapi-proxy'
default_vv_host_dir = '/opt/nginx_proxy/'

def persist_env_vars(args):
    is_https = args.cert and args.key #  and args.ssl_conf
    env_file_content = ''

    rebuild_ts = 0
    if os.path.exists(".env"):
        with open(".env") as f:
            lines = f.read().split('\n')
            for l in lines:
                if '=' not in l:
                    continue
                k,v = l.split('=')
                if k == 'rebuild_flag':
                    rebuild_ts = v
                    print("Retrieved ol  d rebuild_ts {}".format(rebuild_ts))
                    break

    app_name = getattr(args, 'app_name', None)
    if app_name:
        env_file_content += "COMPOSE_PROJECT_NAME={}\n".format(app_name)

    env_file_content += "conf_dir=conf_dir\n"
    if args.server_name:
        env_file_content += 'server_name={}\n'.format(args.server_name)
    env_file_content += "port={}\n".format(args.port)
    env_file_content += "http_listen_port={}\n".format('80' if is_https else args.port)
    env_file_content += 'https_listen_port={}\n'.format(args.port if is_https else '443')
    env_file_content += 'http_redirect_port={}\n'.format("80" if not is_https else (args.port if str(args.port) != "443" else "80"))
    env_file_content += "nginx_conf={}\n".format('nginx_https_template.conf' if is_https else 'nginx_http_template.conf')
    env_file_content += "rebuild_flag={}\n".format(time.time() if args.rebuild else rebuild_ts)
    env_file_content += "vv_proxy={}\n".format(args.vv_proxy)

    open('.env', 'wb').write(env_file_content.encode('utf-8'))


def setup_conf_dir(args):
    conf_dir = os.path.abspath('conf_dir')
    # if args.reset:
    os.system('rm -rf {}'.format(conf_dir))
    os.makedirs(conf_dir, exist_ok=True)

    conf_copy_dir =os.path.join(conf_dir, 'conf')
    os.makedirs(conf_copy_dir, exist_ok=True)
    os.system('cp {} {}'.format(os.path.join(args.nginx_conf_dir, '*.conf'), conf_copy_dir))

    ssl_copy_dir = os.path.join(conf_dir, 'ssl')
    os.makedirs(ssl_copy_dir, exist_ok=True)

    if args.cert:
        os.system('cp {} {}'.format(os.path.abspath(args.cert), os.path.join(ssl_copy_dir, 'cert.pem')))
    if args.key:
        os.system('cp {} {}'.format(os.path.abspath(args.key), os.path.join(ssl_copy_dir, 'privkey.pem')))
    # if args.ssl_conf:
    #    os.system('cp {} {}'.format(os.path.abspath(args.ssl_conf), os.path.join(ssl_copy_dir, 'ssl-options.conf')))

'''
def setup_volume_dirs(args):
    if args.reset:
        os.system('rm -rf {}'.format(args.conf_dir))
    if not os.path.exists(args.conf_dir):
        os.system('mkdir -p {}'.format(args.conf_dir))

    conf_copy_dir =os.path.join(args.conf_dir, 'conf')
    os.system('mkdir -p {}'.format(conf_copy_dir))
    os.system('cp {} {}'.format(os.path.join(args.conf_dir, '*.conf'), conf_copy_dir))

    ssl_copy_dir = os.path.join(args.conf_dir, 'ssl')
    os.system('mkdir -p {}'.format(ssl_copy_dir))
    if args.cert:
        os.system('cp {} {}'.format(os.path.abspath(args.cert), ssl_copy_dir))
    if args.key:
        os.system('cp {} {}'.format(os.path.abspath(args.key), ssl_copy_dir))
    if args.ssl_conf:
        os.system('cp {} {}'.format(os.path.abspath(args.ssl_conf), ssl_copy_dir))
'''

def invoke_docker_compose():
    os.system("docker-compose up -d --build")


def main(argv):
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '-p', '--port', help='port', dest='port', required=False, default='9501'
    )
    parser.add_argument(
        '-n', '--app_name', help='app_name',
        dest='app_name', default=default_app_name, required=False
    )
    parser.add_argument(
        '-r', help='reset', dest='reset', required=False, action='store_true'
    )
    parser.add_argument(
        '-l', '--rebuild', help='force reload', dest='rebuild', required=False, action='store_true'
    )
    parser.add_argument(
        '--server_name', dest='server_name', required=False, default=None)

    parser.add_argument(
        '--cert', dest='cert', required=False, default=None)
    parser.add_argument(
        '--key', dest='key', required=False, default=None)
    # parser.add_argument(
    #    '--ssl_conf', dest='ssl_conf', required=False, default=None)

    parser.add_argument(
      '--nginx_conf_dir', help='nginx config dir',
       dest='nginx_conf_dir', default=None, required=True
    )
    parser.add_argument(
        '--vv_proxy', help='docker proxy common network', dest='vv_proxy', required=False, default='vv_proxy'
    )


    args, unknown = parser.parse_known_args()

    persist_env_vars(args)
    setup_conf_dir(args)
    invoke_docker_compose()


if __name__ == '__main__':
    main(sys.argv[:])
