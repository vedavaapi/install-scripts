#!/usr/bin/env bash
action=$1
rel=$2
docker ps -a| sed -n "2,$ s/^.* \(${rel}_[^ ]*\)$/\1/p" | xargs docker ${action}
