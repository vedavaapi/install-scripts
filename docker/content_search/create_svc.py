import argparse
import json
import sys
import os
import time

import requests

default_app_name = 'content_search'
default_version = 'master'
default_data_dir = '/opt/content_search/data/'


def setup_volume_dirs(host_dir, reset):
    for path in ['es']:
        path = os.path.join(host_dir, path)
        if reset:
            print("Removing {}".format(path))
            os.system('rm -rf {}'.format(path))
        if not os.path.exists(path):
            os.system('mkdir -p {}'.format(path))


def persist_env_vars(args):
    env_file_content = ''

    app_name = getattr(args, 'app_name', None)
    if app_name:
        env_file_content += "COMPOSE_PROJECT_NAME={}\n".format(app_name)

    env_file_content += "version={}\n".format(args.version)
    env_file_content += "port={}\n".format(args.port)
    env_file_content += "url_mount_path={}\n".format(args.url_mount_path)
    env_file_content += "rebuild_flag={}\n".format(time.time() if args.rebuild else 0)
    env_file_content += "rebuild_flag={}\n".format(time.time() if args.rebuild else 0)
    env_file_content += "es_data_dir={}\n".format(
        os.path.join(args.data_dir, 'es')
    )
    env_file_content += "vv_proxy={}\n".format(args.vv_proxy)

    open('.env', 'wb').write(env_file_content.encode('utf-8'))


def create_client(args):
    if not (args.site and args.email and args.pw):
        print('site, email, pw are must to register a client. exiting')
        sys.exit()
    session = requests.session()
    session.post(
        os.path.join(args.site, 'accounts/v1/oauth/signin'),
        data={"email": args.email, "password": args.pw}
    ).raise_for_status()

    client_resp = session.post(os.path.join(args.site, 'accounts/v1/oauth/clients'), data={
        "name": "Anveshin",
        "grant_types": json.dumps(["client_credentials"]),
        "client_type": "private",
    })
    client_resp.raise_for_status()
    client_json  = client_resp.json()

    return {
        "client_id": client_json['client_id'],
        "client_secret": client_json['client_secret']
    }


def gen_client_creds(args):
    client = create_client(args)
    open('client_creds.json', 'wb').write(json.dumps(client).encode('utf-8'))


def gen_app_config(args):
    if os.path.exists('config.json') and not args.reset:
        return
    conf = {
        "VV": {
            "base_url": args.site_proxy or args.site,
            "client_creds_path": "/opt/client_creds.json",
            "atr_path": "/opt/atr.json"
        },
        "ES": {
            "hosts": ["es"]
        }
    }
    open('config.json', 'wb').write(
        json.dumps(conf, indent=2).encode('utf-8'))


def gen_config(args):
    gen_client_creds(args)
    gen_app_config(args)


def invoke_docker_compose():
    ret = os.system("docker-compose up -d --build")
    print(ret)
    return ret


def main(argv):
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '-p', help='port', dest='port', required=False, default='9501'
    )
    parser.add_argument(
        '-n', '--app_name', help='app_name',
        dest='app_name', default=default_app_name, required=False
    )
    parser.add_argument(
        '-V', '--version', help='version/branch/coomit',
        dest='version', default=default_version, required=False
    )
    parser.add_argument(
        '-m', help='url mount path', dest='url_mount_path', required=False, default=''
    )
    parser.add_argument(
        '-d', help='content_search data base-path', dest='data_dir', required=False, default=default_data_dir
    )
    parser.add_argument(
        '-r', help='reset', dest='reset', required=False, action='store_true'
    )
    parser.add_argument(
        '-l', '--rebuild', help='Force rebuild of Docker container',
        dest='rebuild', action="store_true", required=False
    )
    parser.add_argument(
        '--site', help='site url', dest='site', required=False
    )
    parser.add_argument(
        '--site_proxy', help='site proxy url', dest='site_proxy', required=False, default=None
    )
    parser.add_argument(
        '--email', help='email', dest='email', required=False
    )
    parser.add_argument(
        '--pw', help='password', dest='pw', required=False
    )
    parser.add_argument(
        '--vv_proxy', help='docker proxy common network', dest='vv_proxy', required=False, default='vv_proxy'
    )

    args, unknown = parser.parse_known_args()

    setup_volume_dirs(args.data_dir, args.reset)
    persist_env_vars(args)
    gen_config(args)
    return invoke_docker_compose()


if __name__ == '__main__':
    sys.exit(main(sys.argv[:]))
