import json
import re
import subprocess
import os
import sys

import argparse


(docker_dir, fname) = os.path.split(os.path.abspath(__file__))


class cd:
    """Context manager for changing the current working directory"""
    def __init__(self, new_path):
        self.newPath = os.path.expanduser(new_path)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)


def mycheck_output(cmd):
    try:
        shellval = False if (type(cmd) == type([])) else True
        return subprocess.Popen(
            cmd, shell=shellval, stderr=subprocess.PIPE, stdout=subprocess.PIPE).communicate()[0].decode('utf-8')
    except Exception as e:
        print("Error in ", cmd, ": ", e)
        return "error"


def get_ip():
    ip = re.split(
        r'\s', subprocess.check_output(['hostname', '--all-ip-addresses']).decode('utf-8'))[0].strip()
    return ip


class Param(object):

    @classmethod
    def get_args(cls, k, v):
        c1 = '{}{}'.format('-' if len(k) == 1 else '--', k)
        if not v:
            return []

        if isinstance(v, bool):
            if not v:
                return []
            return [c1]

        return [c1, str(v)]


def get_services_conf():
    conf_path = os.path.join(docker_dir, 'services_conf.json')
    print(os.path.abspath(conf_path))
    if not os.path.isfile(conf_path):
        return None
    return json.loads(open(conf_path, 'rb').read().decode('utf-8'))


class LaunchContext(object):

    def __init__(
            self, org=None, root_dir=None,
            rel='prod', hostname=None, scheme='http',
            reset=False, reload=False, proxy_port=None, email=None, password=None, site_url=None, **kwargs):

        self.org = org
        self.root_dir = root_dir
        self.rel = rel

        self.reset = reset
        self.reload = reload

        self.hostname = hostname
        if scheme not in ['http', 'https']:
            print('invalid scheme', file=sys.stderr)
        self.scheme = scheme
        self.proxy_port = proxy_port or ('80' if self.scheme == 'http' else '443')
        self.proxy_network = '{}_vv_proxy'.format(self.rel)

        self.proxy_base = '{}://{}{}'.format(
            self.scheme, self.hostname,
            ':{}'.format(self.proxy_port) if not (
                    (self.scheme == 'http' and self.proxy_port == '80')
                    or (self.scheme == 'https' and self.proxy_port == '443')
            ) else ''
        )

        self.email = email
        self.password = password
        self.site_url = site_url

    def get_args(self, for_create_svc=False):
        arg_props_map = {
            "org": "org", "d": "root_dir", "rel": "rel", "hostname": 'hostname', 'server_name': 'hostname', 'vv_proxy': 'proxy_network'
        }
        args = []
        for arg, prop in arg_props_map.items():
            if for_create_svc and arg in ['org', 'd', 'rel']:
                continue
            v = getattr(self, prop)
            if not v:
                continue
            args.extend(Param.get_args(arg, v))

        args.extend(Param.get_args('r', self.reset))
        args.extend(Param.get_args('l', self.reload))

        return args

    @classmethod
    def request(cls, cur_context=None):
        param_requests = [
            ParamRequest('org', default='vv', prompt='Specify Org prefix'),
            ParamRequest('root_dir', default='/data/vedavaapi', prompt='Specify root vedavaapi installation dir'),
            ParamRequest('rel', default='prod', prompt='Specify release'),
            ParamRequest('hostname', required=True, prompt='Specify internet accessible address', default=get_ip()),
            ParamRequest('scheme', required=False, prompt='Enter Scheme http/https', default='http'),
            ParamRequest('proxy_port', default=None, required=False, prompt='Enter Proxy Port [80 if http, 443 if https]'),
            ParamRequest('site_url', default=None, required=False, prompt='Enter external site_url if site is external'),
            ParamRequest('email', default=None, required=True, prompt='Enter account email'),
            ParamRequest('password', default=None, prompt='Enter account password')
        ]
        resp = {}
        for pr in param_requests:
            try:
                resp[pr.param] = pr.execute(cur_val=getattr(cur_context, pr.param) if cur_context else None)
            except ValueError:
                print(
                    'mandatory field is not given, exiting', file=sys.stderr)
                sys.exit()

        return cls(**resp)

    def json(self):
        context_json = {}
        for prop in ('org', 'root_dir', 'rel', 'hostname', 'scheme', 'proxy_port', 'site_url', 'email', 'password'):
            context_json[prop] = getattr(self, prop)
        return context_json

    @classmethod
    def from_json(cls, context_json):
        return cls(**context_json)


class ServiceContext(object):

    def __init__(self, name, params=None, urls = None):
        self.name = name
        self.params = params or {}
        self.urls = urls

    def get_args(self):
        args = []
        for k, v in self.params.items():
            args.extend(Param.get_args(k, v))
        return args

    def json(self):
        return {
            "name": self.name,
            "params": self.params,
            "urls": self.urls
        }

    @classmethod
    def from_json(cls, context_json):
        if context_json is None:
            return None
        return cls(context_json['name'], context_json.get('params'), urls=context_json.get('urls'))


class ParamRequest(object):

    def __init__(self, param, default=None, required=False, prompt=None, is_bool=False, bool_trues=None):
        self.param = param
        self.default = default
        self.required = required
        self.prompt = prompt or "Enter {}".format(self.param)
        self.is_bool = is_bool
        self.bool_trues = bool_trues or ['y', 'yes']

    def execute(self, cur_val=None):
        default = cur_val or self.default
        val = input(
            "{}{}: ".format(self.prompt, ' [{}]'.format(default) if default else '')
        ) or default
        if self.required and not val:
            raise ValueError(
                '{} is required parameter'.format(self.param))
        if self.is_bool:
            val = val in self.bool_trues
        return val


class ServiceContextRequest(object):

    def __init__(self, svc_name):
        self.svc_name = svc_name
        self.param_requests = []
        self.rt_param_requests = []

    @classmethod
    def from_param_descriptors(cls, svc_name, descrs, rt_descrs):
        context_request = cls(svc_name)
        context_request.param_requests.extend([
            ParamRequest(*descr) for descr in descrs
        ])
        context_request.rt_param_requests.extend([
            ParamRequest(*descr) for descr in rt_descrs or []
        ])
        return context_request

    def execute(self, runtime=False, context=None, re_config=False):
        requests = self.rt_param_requests if runtime else self.param_requests
        context = context or ServiceContext(self.svc_name)

        for p_request in requests:  # type: ParamRequest
            if p_request.param in context.params and not re_config:
                continue
            val = p_request.execute(
                cur_val=context.params.get(p_request.param))
            if val is None:
                continue
            context.params[p_request.param] = val

        return context


def get_svc_context_requests():
    svc_param_descrs = {
        "vvsite": [
            ("gcreds", None, False, 'Enter path to google credentials'),
            ("V", "master", False, "Enter vv site version"),
            ("objstore-files-dir", None, False,
                'Enter objstore files dir path[optional]')
        ],
        "kaveri": [
            ("V", "master", False, "Enter kaveri version"),
            ("def-apps", 'true', False, "Enter def_apps_mode")
        ],
        "console": [
            ("V", "master", False, "Enter console version")
        ],
        "mirador": [
            ("V", "master", False, "Enter mirador version")
        ],
        "tessocr": [
            ("V", "master", False, "Enter version")
        ],
        "googleocr": [
            ("creds", None, True, "Enter path to google cloud service account creds"),
            ("V", "master", False, "Enter version")
        ],
        "sanskritocr": [
            ("V", "master", False, "Enter version")
        ],
        "layout_detector": [
            ("t", None, False, "Enter path to trained model")
        ],
        "iiith_contour_detector": [
            ("t", None, False, "Enter path to trained model")
        ],
        "textdocs": [
            ("V", "master", False, "Enter textdocs version")
        ],
        "textract": [
            ("V", "master", False, "Enter textract-ui version")
        ],
        "ocreditor": [
            ("V", "master", False, "Enter proofreader version")
        ],
        "sling": [

        ],
        "proxy": [
            #  ('port', None, False, "Enter Proxy Port"),
            ("cert", None, False, "Enter SSL certificate path"),
            ("key", None, False, "Enter SSL CERT KEY path"),
            #  ("ssl_conf", None, False, "Enter SSL options path"),
        ],
        "content_search": [
            ("V", "master", False, "Enter content_search version")
        ]
    }

    svc_rt_param_descrs = {
        "console": [
            ("site", None, True, "Enter accessible site url"),
            ("email", None, True, "Enter email id for site"),
            ("pw", None, True, "Enter password"),
            ("depl", None, False, 'enter deployment url'),
            ("site_proxy", None, False, "Enter site proxy url"),
        ],
        "kaveri": [
            ("site", None, True, "Enter accessible site url"),
            ("email", None, True, "Enter email id for site"),
            ("pw", None, True, "Enter password"),
            ("depl", None, False, 'enter deployment url'),
            ("site_proxy", None, False, "Enter site proxy url"),
        ],
        "content_search": [
            ("site", None, True, "Enter accessible site url"),
            ("email", None, True, "Enter email id for site"),
            ("pw", None, True, "Enter password"),
            ("site_proxy", None, False, "Enter site proxy url"),
        ]
    }

    return dict(
        (name, ServiceContextRequest.from_param_descriptors(
            name, svc_param_descrs.get(name, []), svc_rt_param_descrs.get(name, []))
         )
        for name in svc_param_descrs
    )


class LaunchConfig(object):

    def __init__(self, launch_context: LaunchContext, services: list, svc_contexts: dict):
        self.launch_context = launch_context
        self.services = services
        self.svc_contexts = svc_contexts
        self.svc_launch_confs = get_services_conf()

        for svc in self.services:
            if svc not in self.svc_contexts:
                self.svc_contexts[svc] = ServiceContext(svc)

    def json(self):
        return {
            "launch_context": self.launch_context.json(),
            "services": self.services,
            "svc_contexts": dict((svc, sc.json() if sc else None) for (svc, sc) in self.svc_contexts.items())
        }

    @classmethod
    def from_json(cls, launch_conf_json):
        if not launch_conf_json or 'launch_context' not in launch_conf_json:
            print('invalid config file')
            sys.exit(1)
        launch_context = LaunchContext.from_json(launch_conf_json['launch_context'])
        services = launch_conf_json['services']
        svc_contexts = dict(
            (svc, ServiceContext.from_json(sc_json))
            for (svc, sc_json) in launch_conf_json.get('svc_contexts').items()
        )
        return cls(launch_context, services, svc_contexts)

    @classmethod
    def request(cls, svcs):

        launch_context = LaunchContext.request()
        svc_contexts = {}

        svc_context_requests = get_svc_context_requests()
        for svc in svcs:
            sc_request = svc_context_requests.get(svc, None)
            if not sc_request:
                continue
            print('\n\n {}\n========================='.format(svc))
            try:
                sc = sc_request.execute()
            except ValueError:
                sc = None
            svc_contexts[svc] = sc

        return cls(launch_context, svcs, svc_contexts)


"""
typical launch flow
========================
1: cleanup existing
2: launch site
3: bootstrap root admin
4: launch console
5: launch segmenters
6: launch textdocs, proofreader
7: register apps
8: generate server config
9: symlink server config

"""


class Launcher(object):

    REL_PORT_BASE_MAP = {
        "dev": 6000,
        "stage": 7000,
        "prod": 9000
    }

    def __init__(self, launch_config: LaunchConfig):
        self.launch_config = launch_config
        self.sc_requests = get_svc_context_requests()

        self.runtime_context = {}
        self.site_direct_url = None
        self.site_proxy_url = None

        #  self.launch_infos = {}
        self.launch_infos = dict(
            (svc, self._get_svc_launch_info(svc)) for svc in self.launch_config.services)

    def attach_runtime_context(self, svc):
        sc = self.launch_config.svc_contexts[svc]  # type: ServiceContext
        sc_request = self.sc_requests.get(svc)
        if not sc_request or not sc_request.rt_param_requests:
            return
        got_all = True
        for pr in sc_request.rt_param_requests:
            if pr.param in sc.params:
                continue
            if pr.param not in self.runtime_context:
                got_all = False
                continue
            sc.params[pr.param] = self.runtime_context[pr.param]

        if not got_all:
            print('\n\n{}\n======================='.format(svc))
            sc_request.execute(runtime=True, context=sc)
        return

    def get_launch_args(self, svcs: list, for_create_svc=False):
        args = ['-s', ','.join(svcs)] if not for_create_svc else []
        args.extend(self.launch_config.launch_context.get_args(for_create_svc=for_create_svc))
        for svc in svcs:
            sc = self.launch_config.svc_contexts[svc]  # type: ServiceContext
            if sc is not None:
                args.extend(sc.get_args())
        return args

    def _setup_launch_run_dir(self):
        data_root_dir = self.launch_config.launch_context.root_dir
        rel = self.launch_config.launch_context.rel

        run_dir = os.path.join(data_root_dir, rel, 'containers/run')
        os.makedirs(run_dir, exist_ok=True)
        with cd(docker_dir):
            os.system('cp -r ./* {}'.format(run_dir))

    def _get_svc_launch_info(self, svc):
        svc_launch_conf = self.launch_config.svc_launch_confs.get(svc, {})
        rel = self.launch_config.launch_context.rel
        port = (self.REL_PORT_BASE_MAP[rel] + svc_launch_conf['port']) if 'port' in svc_launch_conf else None
        mount_path = ((
            '/apps' if svc_launch_conf.get('is_app') else ''
        ) + svc_launch_conf['mount_base_path']) if svc_launch_conf.get('mount_base_path') else None

        if mount_path:
            svc_direct_url = 'http://{}:{}/{}'.format(
                self.launch_config.launch_context.hostname,
                port, mount_path.lstrip('/')
            ).rstrip('/')

            svc__proxy_url = '{}/{}'.format(
                self.launch_config.launch_context.proxy_base,
                mount_path.lstrip('/')
            ).rstrip('/')

        dc_project_name = '{}1{}1{}'.format(
            self.launch_config.launch_context.org,
            rel, svc)
        info = {
            "project": dc_project_name,
            "mount_path": mount_path,
            "port": port, "svc": svc,
            "behind_internal_proxy": svc_launch_conf.get('behind_internal_proxy', True),
        }
        if port and mount_path:
            # noinspection PyUnboundLocalVariable
            info['urls'] = {"direct": svc_direct_url, "proxy": svc__proxy_url}
        return info

    def _launch_svc(self, svc):
        if not os.path.isdir(os.path.join(docker_dir, svc)):
            print('No Such Service', file=sys.stderr)
            return

        data_root_dir = self.launch_config.launch_context.root_dir
        rel = self.launch_config.launch_context.rel

        svc_run_dir = os.path.join(data_root_dir, rel, 'containers/run', svc)
        svc_vols_dir = os.path.join(data_root_dir, rel, 'containers/vols', svc)
        #  os.makedirs(svc_run_dir, exist_ok=True)
        os.makedirs(svc_vols_dir, exist_ok=True)

        launch_info = self._get_svc_launch_info(svc)
        computed_args_dict = {
            "d": svc_vols_dir, "p": launch_info['port'],
            "m": launch_info['mount_path'], "n": launch_info['project']
        }

        args = []
        args.extend(self.get_launch_args([svc], for_create_svc=True))
        for key, val in computed_args_dict.items():
            args.extend(Param.get_args(key, val))
        
        with cd(svc_run_dir):
            command = "python3 create_svc.py {args}".format(args=' '.join(args))
            print(command)
            status = os.system(command)
            print(status)
            if int(status) != 0:
                print('error in launching {}'.format(svc))
                sys.exit(1)

        return launch_info

    def launch_svc(self, svc):
        if svc not in self.launch_config.services:
            print('service {} not defined'.format(svc), file=sys.stderr)
            return
        if self.launch_config.svc_contexts[svc] is None:
            print('skipping service {}'.format(svc), file=sys.stderr)
            return
        try:
            self.attach_runtime_context(svc)
            should_proceed = self._custom_action_before(svc, self.launch_config.svc_contexts[svc])
            if not should_proceed:
                return
        except ValueError:
            print('skipping service {}'.format(svc))
            return

        launch_info = self._launch_svc(svc)
        self._custom_action_after(svc, launch_info)
        return launch_info

    def _custom_action_after(self, svc, launch_info):
        if svc == 'vvsite':
            #  self._extract_site_urls(launch_info)
            self._bootstrap_root_admin(launch_info)

    def _custom_action_before(self, svc, sc: ServiceContext):
        if svc == 'proxy':
            scheme = self.launch_config.launch_context.scheme
            nginx_conf_dir = os.path.join(
                self.launch_config.launch_context.root_dir,
                'nginx/vv_proxy', self.launch_config.launch_context.rel,
                scheme
            )
            sc.params['nginx_conf_dir'] = nginx_conf_dir
            if not sc.params.get('port'):
                sc.params['port'] = self.launch_config.launch_context.proxy_port

            if scheme == 'https' and (not sc.params.get('cert') or not sc.params.get('key')):
                print('ssl cert, key are required for https proxy. skipping proxy service', file=sys.stderr)
                return False
            return True

        return True

    def _load_runtime_context(self):
        if 'vvsite' in self.launch_config.svc_contexts:
            self.runtime_context.update({
                "site": self.launch_config.svc_contexts['vvsite'].urls['direct'],
                "site_proxy": self.launch_config.svc_contexts['vvsite'].urls['proxy'],
            })
            self.site_direct_url = self.runtime_context['site']
            self.site_proxy_url = self.runtime_context['site_proxy']
        elif self.launch_config.launch_context.site_url:
            self.runtime_context.update({
                "site": self.launch_config.launch_context.site_url,
                "site_proxy": self.launch_config.launch_context.site_url,
            })
        self.runtime_context['depl'] = self.launch_config.launch_context.proxy_base


    def _ask_creds(self):
        if not self.runtime_context.get('email') or not self.runtime_context.get('pw'):
            self.runtime_context['email'] = self.launch_config.launch_context.email or ParamRequest('email', None, True, prompt='Enter root admin email').execute()
            self.runtime_context['pw'] = self.launch_config.launch_context.password or ParamRequest('pw', None, True, 'Enter admin password').execute()

    def _bootstrap_root_admin(self, *args):
        import requests
        self._ask_creds()
        setup_uri = os.path.join(self.site_direct_url, 'accounts/v1/setup/admin')
        # noinspection PyUnusedLocal
        resp = requests.post(
            setup_uri, data={"email": self.runtime_context.get('email'), "password": self.runtime_context.get('pw')}
        )
        # resp.raise_for_status()

    def launch(self, services):
        self._setup_launch_run_dir()
        self._load_runtime_context()
        self._ask_creds()
        for svc in services:
            print('\n\nlaunching {}\n=================\n'.format(svc))
            launch_info = self.launch_svc(svc)
            print({'launch_info': launch_info})
            self.launch_infos[svc] = launch_info
        self.register_svcs()
        print("You can now access Vedavaapi console:")
        print("    {}".format(self.launch_config.launch_context.proxy_base))

    @classmethod
    def _gen_apache_conf(cls, svc, https=False, docker_proxy=False, forward_port=None, proxy_port=None, mount_path='/', behind_internal_proxy=True):
        if mount_path is None or (not docker_proxy and not forward_port):
            return None
        conf = """
            <Location {mount_path}>
                ProxyPass http://{hostname}:{port}{mount_path}
                ProxyPassReverse http://{hostname}:{port}{mount_path}
                ProxyPreserveHost on
                RequestHeader set X-Forwarded-Port {proxy_port}
                RequestHeader set X-Forwarded-Scheme {proxy_scheme}
            </Location>
        """.format(
            mount_path=mount_path,
            hostname=svc.replace('_', '-') if docker_proxy else 'localhost',
            port=str(80 if (docker_proxy and behind_internal_proxy) else forward_port),
            proxy_port=str(proxy_port or (443 if https else 80)),
            proxy_scheme='https' if https else 'http'
        )
        return conf

    def _gen_server_conf(self, server, svc, **kwargs):
        if server == 'apache2':
            return self._gen_apache_conf(svc, **kwargs)
        elif server == 'nginx':
            return self._gen_nginx_conf(svc, **kwargs)
        return None

    # noinspection PyUnusedLocal
    @classmethod
    def _gen_nginx_conf(cls, svc, https=False, docker_proxy=False, forward_port=None, proxy_port=None, mount_path='/', behind_internal_proxy=True):
        if mount_path is None or (not docker_proxy and not forward_port):
            return None
        is_standard_port = (https and str(proxy_port) == '443') or (not https and str(proxy_port) == '80')

        conf = """
            location {mount_path} {{
                proxy_pass http://{hostname}:{port};
                # proxy_redirect off;
                proxy_set_header X-Forwarded-Proto $scheme;
                proxy_set_header X-Forwarded-Scheme $scheme;
                proxy_set_header Host $host;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header X-Forwarded-Host $host{port_extn};
                proxy_set_header X-Forwarded-Port $server_port;
            }}
        """.format(
            mount_path=mount_path,
            hostname=svc.replace('_', '-') if docker_proxy else 'localhost',
            port=str(80 if (docker_proxy and behind_internal_proxy) else forward_port),
            port_extn=':$server_port' if not  is_standard_port else ''
        )
        return conf

    def _gen_host_server_conf_wrapper(self):
        if 'proxy' not in os.listdir(docker_dir):
            return
        lc = self.launch_config.launch_context
        proxy_svc_context = self.launch_config.svc_contexts.get('proxy')  # type: ServiceContext
        apache2_host_dir = os.path.join(lc.root_dir, 'apache2', 'host', lc.rel)
        conf_template_file = 'nginx_https_template.conf' if lc.scheme == 'https' else 'nginx_http_template.conf'
        conf_template = open(
            os.path.join(docker_dir, 'proxy', conf_template_file), 'rb'
        ).read().decode('utf-8').replace('{port}', lc.proxy_port).replace('{server_name}', lc.hostname).replace(
            '/etc/apache2/sites-enabled/vedavaapi/*.conf',
            os.path.join(apache2_host_dir, lc.scheme, 'vedavaapi.conf')
        )

        if proxy_svc_context:
            proxy_params = proxy_svc_context.params
            if False not in [p in proxy_params for p in ['key', 'cert', 'ssl_conf']]:
                conf_template = conf_template.replace(
                    "/conf_dir/ssl/cert.pem", proxy_params['cert']
                ).replace(
                    '/conf_dir/ssl/privkey.pem', proxy_params['key']
                ).replace(
                    '/conf_dir/ssl/ssl-options.conf', proxy_params['ssl_conf']
                )

        host_conf_path = os.path.join(apache2_host_dir, lc.scheme, 'host.conf')
        open(host_conf_path, 'wb').write(conf_template.encode('utf-8'))


    def gen_server_conf(self):
        for server in ('apache2', 'nginx'):
            for proxy_machine in ('host', 'vv_proxy'):

                rel = self.launch_config.launch_context.rel
                for scheme in ('http', 'https'):

                    conf_dir = os.path.join(
                        self.launch_config.launch_context.root_dir,
                        server, proxy_machine, rel, scheme
                    )
                    os.makedirs(conf_dir, exist_ok=True)
                    os.system('sudo rm -rf {}/*.conf'.format(conf_dir))

                    conf = ''
                    for svc in Commander.all_services:
                        if svc == 'proxy':
                            continue
                        launch_info = self._get_svc_launch_info(svc)
                        server_conf = self._gen_server_conf(
                            server, svc,
                            https=(scheme == 'https'),
                            docker_proxy=(proxy_machine == 'vv_proxy'),
                            forward_port=launch_info['port'],
                            proxy_port=self.launch_config.launch_context.proxy_port,
                            mount_path=launch_info['mount_path'],
                            behind_internal_proxy=launch_info.get('behind_internal_proxy', True),
                        )
                        if server_conf is None:
                            continue
                        conf += server_conf

                    open(os.path.join(conf_dir, 'vedavaapi.conf'), 'wb').write(conf.encode('utf-8'))
        self._gen_host_server_conf_wrapper()
        return None

    def _get_signed_in_session(self, site_url, email, password):
        import requests
        sign_in_url = os.path.join(site_url, 'accounts/v1/oauth/signin')
        session = requests.Session()
        session.post(
            sign_in_url, data={"email": email, "password": password}
        ).raise_for_status()
        return session

    def _get_token(self, site_url, session):
        token_url = os.path.join(site_url, 'accounts/v1/me/token')
        token_resp = session.get(token_url)
        token_resp.raise_for_status()
        token = token_resp.json()['access_token']
        return token

    def _cleanup_services(self, site_url, access_token):
        resources_url = os.path.join(site_url, 'objstore/v1/resources')
        import requests
        resp = requests.get(
            resources_url, params={"selector_doc": json.dumps({"jsonClass": "Service"}), "projection": json.dumps({"_id": 1})}
        )
        resp.raise_for_status()
        services = resp.json()['items']
        svc_ids = [svc['_id'] for svc in services]
        requests.delete(
            resources_url,
            data={'resource_ids' : json.dumps(svc_ids)}, headers={"Authorization": 'Bearer {}'.format(access_token)}
        ).raise_for_status()

    def register_svcs(self):
        print('\n\nRegistration\n================')
        site_url = self.site_direct_url or self.site_proxy_url
        if not site_url:
            if 'vvsite' in self.launch_config.svc_contexts:
                site_url = self.launch_config.svc_contexts['vvsite'].urls['direct']
            elif self.launch_config.launch_context.site_url:
                site_url = self.launch_config.launch_context.site_url
            else:
                site_url = self.site_proxy_url = self.site_direct_url = ParamRequest(
                    'site', required=True,
                    prompt='Enter Site URL you want services registered to'
                ).execute()
        self._ask_creds()

        resources_url = os.path.join(site_url, 'objstore/v1/resources')
        acls_url = os.path.join(site_url, 'acls/v1/{resource_id}')

        session = self._get_signed_in_session(site_url, self.runtime_context['email'], self.runtime_context['pw'])
        token = self._get_token(site_url, session)

        svc_jsons = []
        for svc, launch_info in self.launch_infos.items():
            svc_launch_conf = self.launch_config.svc_launch_confs.get(svc)
            if not svc_launch_conf or not launch_info:
                continue
            if not svc_launch_conf.get('is_app'):
                continue

            origin = self.launch_config.launch_context.proxy_base
            app_confs = svc_launch_conf['apps']

            for app_conf in app_confs:
                svc_json = app_conf.copy()
                svc_json.update({
                    "jsonClass": "Service",
                    "url": '{}/{}/{}'.format(
                        origin.rstrip('/'),
                        launch_info['mount_path'].lstrip('/').rstrip('/'),
                        svc_json.get('url', '').lstrip('/').rstrip('/'))
                })
                svc_jsons.append(svc_json)

        apps_resp = session.post(
            resources_url, data={
                "resource_jsons": json.dumps(svc_jsons),
                'upsert': 'true', "return_projection": json.dumps({"_id": 1})
            },
            headers={"Authorization": 'Bearer {}'.format(token)}
        )
        apps_resp.raise_for_status()

        for app in apps_resp.json():
            session.post(acls_url.format(
                resource_id=app['_id']),
                data={"actions": json.dumps(['read']), "control": 'grant', "user_ids": json.dumps(['*'])},
                headers={"Authorization": 'Bearer {}'.format(token)}
            ).raise_for_status()

        print('services registered successfully')


class Commander(object):
    all_services = [
        'proxy', 'vvsite', 'kaveri', 'console', 'mirador',
        'googleocr', 'sanskritocr', 'segmenter', 'layout_detector', "iiith_contour_detector", 'tessocr', 'textract',
        'textdocs', 'ocreditor', "sling", "content_search"
    ]
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-c', '--conf', help='configuration file path', dest='conf', required=True)
    parser.add_argument(
        '-s', '--services', help='services to b launched', dest='services', required=False, default=None)
    parser.add_argument(
        '-r', '--reset', help='reset', dest='reset', required=False, default=False, action='store_true')
    parser.add_argument(
        '-u', '--update', help='update conf', dest='update', required=False, default=False, action='store_true')
    parser.add_argument(
        '-l', '--rebuild', help='rebuild', required=False, default=False, dest='rebuild', action='store_true')
    parser.add_argument(
        'command', help='one of {up, down, start, stop, restart, gen}')

    def __init__(self, argv):
        self.argv = argv
        self.args, self.unknown = self.parser.parse_known_args(self.argv)
        self.implicit_all = not self.args.services
        self.args.services = re.split(r'\s*,\s*', self.args.services) if self.args.services else self.args.services
        self.launch_config = None

    def command(self):
        if self.args.command not in ('up', 'down', 'start', 'stop', 'restart', 'gen', 'pconf', 'register'):
            print('invalid command', file=sys.stderr)
            self.parser.print_help()
            return
        getattr(self, self.args.command)()

    def _set_flags(self):
        if not self.launch_config:
            return
        launch_context = self.launch_config.launch_context
        launch_context.reload = self.args.rebuild
        launch_context.reset = self.args.reset

    def _load_conf(self):
        if self.launch_config:
            return
        if not os.path.isfile(self.args.conf):
            print('conf file does not exist', file=sys.stderr)
            sys.exit(1)
        print(self.args.conf, open(self.args.conf, 'rb').read().decode('utf-8'))
        config_json = json.loads(
            open(self.args.conf, 'rb').read().decode('utf-8'))
        self.launch_config = LaunchConfig.from_json(config_json)
        self._set_flags()
        if not self.args.services:
            self.args.services = self.launch_config.services

    def _attach_svc_urls(self):
        if not self.launch_config:
            return
        launcher = Launcher(self.launch_config)
        for svc in self.launch_config.services:
            if svc =='proxy':
                continue
            svc_info = launcher._get_svc_launch_info(svc)
            if svc_info is None or not self.launch_config.svc_contexts[svc]:
                continue
            self.launch_config.svc_contexts[svc].urls = svc_info.get('urls')

    def _gen_conf(self):
        self.args.services = self.args.services or self.all_services
        os.makedirs(
            os.path.dirname(os.path.abspath(self.args.conf)), exist_ok=True
        )
        self.launch_config = LaunchConfig.request(self.args.services)
        self._attach_svc_urls()

        config_file = open(self.args.conf, 'wb')
        config_file.write(
            json.dumps(self.launch_config.json(), ensure_ascii=False, indent=2).encode('utf-8')
        )
        self._set_flags()

        launcher = Launcher(self.launch_config)
        launcher.gen_server_conf()
        return self.launch_config

    def _update_conf(self):
        if not self.launch_config:
            return
        prev_launch_context = self.launch_config.launch_context
        self.launch_config.launch_context = LaunchContext.request(
            cur_context=prev_launch_context
        )
        self.launch_config.launch_context.reset = prev_launch_context.reset
        self.launch_config.launch_context.reload = prev_launch_context.reload

        if self.args.services:
            for svc in self.args.services:
                if svc not in self.launch_config.services:
                    self.launch_config.services.append(svc)

        sc_requests = get_svc_context_requests()
        for svc in self.args.services or self.all_services:
            sc_request = sc_requests.get(svc)
            if not sc_request:
                continue
            print('\n\n{}\n======================='.format(svc))
            svc_context = self.launch_config.svc_contexts.get(svc)
            try:
                svc_context = sc_request.execute(re_config=True, context=svc_context)
            except ValueError:
                pass
            self.launch_config.svc_contexts[svc] = svc_context
        self._attach_svc_urls()
        config_file = open(self.args.conf, 'wb')
        config_file.write(
            json.dumps(self.launch_config.json(), ensure_ascii=False, indent=2).encode('utf-8')
        )
        if self.implicit_all:
            launcher = Launcher(self.launch_config)
            launcher.gen_server_conf()

    def gen(self):
        if not self.args.update:
            self._gen_conf()
            return
        if not os.path.isfile(self.args.conf):
            self._gen_conf()
            return
        self._load_conf()
        self._update_conf()

    def _ensure_conf(self):
        if self.launch_config:
            return
        if not os.path.isfile(self.args.conf) or self.args.update:
            self.gen()
        else:
            self._load_conf()

    def up(self):
        self._ensure_conf()
        if self.args.reset:
            self.down()
        os.system('docker network create {}'.format(self.launch_config.launch_context.proxy_network))
        launcher = Launcher(self.launch_config)
        launcher.launch(self.args.services)

    def _get_svc_dirs(self, svc):
        lc = self.launch_config.launch_context
        svc_run_dir = os.path.join(
            lc.root_dir, lc.rel, 'containers/run/', svc)
        svc_vols_dir = os.path.join(lc.root_dir, lc.rel, 'containers/vols/', svc)
        return svc_run_dir, svc_vols_dir

    def down(self):
        self._load_conf()

        for svc in self.args.services:
            print('removing svc {}'.format(svc), file=sys.stderr)
            svc_run_dir, svc_vols_dir = self._get_svc_dirs(svc)
            try:
                with cd(svc_run_dir):
                    mycheck_output('docker-compose down')
            except FileNotFoundError:
                print('{} service definitions does not exists'.format(svc))

            if self.args.reset:
                mycheck_output('sudo rm -rf "{}" "{}"'.format(svc_run_dir, svc_vols_dir))

        if self.implicit_all:
            print('removing proxy network', file=sys.stderr)
            os.system('docker network rm {}'.format(self.launch_config.launch_context.proxy_network))

    def start(self):
        self._load_conf()
        for svc in self.args.services:
            print('starting svc {}'.format(svc), file=sys.stderr)
            svc_run_dir, svc_vols_dir = self._get_svc_dirs(svc)
            try:
                with cd(svc_run_dir):
                    mycheck_output('docker-compose start')
            except FileNotFoundError:
                print('cannot start service {}, as defs does not exist'.format(svc))

    def stop(self):
        self._load_conf()
        for svc in self.args.services:
            print('stopping svc {}'.format(svc), file=sys.stderr)
            svc_run_dir, svc_vols_dir = self._get_svc_dirs(svc)
            try:
                with cd(svc_run_dir):
                    mycheck_output('docker-compose stop')
            except FileNotFoundError:
                print('cannot stop service {}, as defs does not exist'.format(svc))

    def restart(self):
        self.stop()
        self.start()

    def pconf(self):
        self._load_conf()
        launcher = Launcher(self.launch_config)
        launcher.gen_server_conf()

    def register(self):
        self._load_conf()
        launcher = Launcher(self.launch_config)
        site_url = self.launch_config.svc_contexts['vvsite'].urls['direct'] if 'vvsite' in self.launch_config.svc_contexts else self.launch_config.launch_context.site_url
        if not site_url:
            print('no site url', file=sys.stderr)

        if self.args.reset:
            session = launcher._get_signed_in_session(
                site_url,
                self.launch_config.launch_context.email,
                self.launch_config.launch_context.password
            )
            token = launcher._get_token(site_url, session)
            launcher._cleanup_services(site_url, token)

        launcher.register_svcs()


if __name__ == '__main__':

    commander = Commander(sys.argv[1:])
    commander.command()
