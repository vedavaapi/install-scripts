FROM ubuntu:latest
ARG DEBIAN_FRONTEND=noninteractive

# first install python3, pip3, mongodb, apache
RUN apt-get -yq update
RUN apt-get -yq install nano git python3 python3-pip
RUN apt-get install -yq apt-utils vim curl apache2 apache2-utils libexpat1 ssl-cert libapache2-mod-wsgi-py3
RUN a2enmod wsgi

# install dependencies
RUN apt-get -yq install python3-requests; \
	apt-get -yq install python3-flask; \
	pip3 install flask-cors flask-restplus;

RUN pip3 install celery[redis] torch numpy argparse
RUN pip3 install "opencv-python<=3.4.8"
RUN apt-get -yq install libsm6 libxext6 libfontconfig1 libxrender1
RUN pip3 install scikit-image
RUN pip3 install werkzeug==0.16.0

# now clone vedavaapi repositories
ENV SRC_DIR "/opt/iiithcd/src"
RUN mkdir -p $SRC_DIR;

RUN chown www-data ${SRC_DIR}
USER www-data

WORKDIR $SRC_DIR

# get git repos
ARG rebuild_flag
ARG version
RUN echo "iiithcd version -:- ${version}" > version-info.txt
ARG git_repos_clone_script="clone_git_repos.sh"
COPY ${git_repos_clone_script} ./
RUN bash ${git_repos_clone_script} ${version}
COPY model.pth Semi-Supervised-Bounding-Box-Predictor/

ENV PYTHONPATH "${PYTHONPATH}:${SRC_DIR}/iiith-contour-detector:${SRC_DIR}/vv-client-python:${SRC_DIR}/Semi-Supervised-Bounding-Box-Predictor:${SRC_DIR}/libtextract"

# symlink apache-conf
USER root
ARG sa_http_conf_link_path="/etc/apache2/sites-available/app.conf"
COPY apache_conf_template.conf "${sa_http_conf_link_path}"
RUN sed -i "s#{APP_DIR}#${SRC_DIR}/iiith-contour-detector#g;" "${sa_http_conf_link_path}"

ARG url_mount_path
RUN sed -i "s#{MOUNT_PATH}#${url_mount_path}#g;" "${sa_http_conf_link_path}"
RUN ln -s ${sa_http_conf_link_path} "/etc/apache2/sites-enabled/app.conf";

ENV APACHE_RUN_DIR /var/run/apache2
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2
RUN apache2ctl start
