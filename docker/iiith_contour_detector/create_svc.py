import argparse
import sys
import os
import time

default_app_name = 'iiith_contour_detector'
default_version = 'master'
default_vv_host_dir = '/opt/iiithcd/'

def persist_env_vars(args):
    env_file_content = ''

    app_name = getattr(args, 'app_name', None)
    if app_name:
        env_file_content += "COMPOSE_PROJECT_NAME={}\n".format(app_name)

    env_file_content += "version={}\n".format(args.version)
    env_file_content += "port={}\n".format(args.port)
    env_file_content += "url_mount_path={}\n".format(args.url_mount_path)
    env_file_content += "iiithcd_data={}\n".format(args.iiithcd_data)
    env_file_content += "rebuild_flag={}\n".format(time.time() if args.rebuild else 0)
    env_file_content += "vv_proxy={}\n".format(args.vv_proxy)

    open('.env', 'wb').write(env_file_content.encode('utf-8'))


def get_trained_data_model(args):
    trained_data_model_path = os.path.abspath(args.trained_model)
    if os.path.exists(trained_data_model_path):
        os.system('cp {} model.pth'.format(trained_data_model_path))
    else:
        gdrive_file_id = '1gIHE40VX-udzC9CuvcZXSvjVqa4vVEhd'
        print('downloading pretrained model.')
        os.system(
            r'''wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id={FILEID}' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id={FILEID}" -O {FILENAME} && rm -rf /tmp/cookies.txt'''.format(FILEID=gdrive_file_id, FILENAME='model.pth')
        )


def setup_volume_dirs(args):
    if args.reset:
        os.system('rm -rf {}'.format(args.iiithcd_data))
    if not os.path.exists(args.iiithcd_data):
        os.system('mkdir -p {}'.format(args.iiithcd_data))


def invoke_docker_compose():
    os.system("docker-compose up -d --build")


def main(argv):
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '-p', help='port', dest='port', required=False, default='9501'
    )
    parser.add_argument(
        '-n', '--app_name', help='app_name',
        dest='app_name', default=default_app_name, required=False
    )
    parser.add_argument(
        '-V', '--version', help='version/branch/coomit',
        dest='version', default=default_version, required=False
    )
    parser.add_argument(
        '-m', help='url mount path', dest='url_mount_path', required=False, default=''
    )
    parser.add_argument(
        '-d', '--host_dir', help='iiithcd host_dir',
        dest='host_dir', default=default_vv_host_dir, required=False
    )
    parser.add_argument(
        '-t', help='trained_model', dest='trained_model', required=False, default='model.pth'
    )
    parser.add_argument(
        '-r', help='reset', dest='reset', required=False, action='store_true'
    )
    parser.add_argument(
        '-l', '--rebuild', help='Force rebuild of Docker container',
        dest='rebuild', action="store_true", required=False
    )
    parser.add_argument(
        '--vv_proxy', help='docker proxy common network', dest='vv_proxy', required=False, default='vv_proxy'
    )

    args, unknown = parser.parse_known_args()
    args.iiithcd_data = os.path.join(args.host_dir, args.app_name)

    persist_env_vars(args)
    get_trained_data_model(args)
    setup_volume_dirs(args)
    invoke_docker_compose()


if __name__ == '__main__':
    main(sys.argv[:])
