#!/usr/bin/env bash
version=$1
echo "Deploying Software Version ${version}"
declare repo_names=("apps/textract/iiith-contour-detector" "libs/libtextract" "libs/vv-client-python")
for repo in "${repo_names[@]}"; do
	giturl="https://gitlab.com/vedavaapi/${repo}.git"
	git clone ${giturl}
	cd $(echo "${repo}" | sed 's#^.*/##;')
	git checkout ${version}
	cd -
done

git clone --depth 1 https://github.com/ihdia/Semi-Supervised-Bounding-Box-Predictor.git
