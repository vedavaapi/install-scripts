import argparse
import json
import re
import subprocess
import sys
import os
import time
import secrets
import requests

default_app_name = 'kaveri'
default_version = 'master'

def persist_env_vars(args):
    env_file_content = ''

    rebuild_ts = 0
    if os.path.exists(".env"):
        with open(".env") as f:
            lines = f.read().split('\n')
            for l in lines:
                if '=' not in l:
                    continue
                k,v = l.split('=')
                if k == 'rebuild_flag':
                    rebuild_ts = v
                    print("Retrieved ol  d rebuild_ts {}".format(rebuild_ts))
                    break

    app_name = getattr(args, 'app_name', None)
    if app_name:
        env_file_content += "COMPOSE_PROJECT_NAME={}\n".format(app_name)

    env_file_content += "mount_path={}\n".format('/' + args.mount_path.lstrip('/'))
    env_file_content += "version={}\n".format(args.version)
    env_file_content += "port={}\n".format(args.port)
    # env_file_content += "distdir={}\n".format(args.distdir)
    env_file_content += "rebuild_flag={}\n".format(time.time() if args.rebuild else rebuild_ts)
    env_file_content += "vv_proxy={}\n".format(args.vv_proxy)

    open('.env', 'wb').write(env_file_content.encode('utf-8'))


def compute_api_oauth_callback_uris(args):
    base_urls = ["http://localhost:{}".format(args.port)]
    if(args.hostname):
        base_urls.append("http://{}:{}".format(args.hostname, args.port))
    ip_addr = re.split(
        '\s', subprocess.check_output(['hostname', '--all-ip-addresses']).decode('utf-8'))[0].strip()
    base_urls.append('http://{}:{}'.format(ip_addr, args.port))
    if args.depl:
        base_urls.append(args.depl.rstrip('/'))

    callback_urls = [
        os.path.join(b, args.mount_path.lstrip('/'), 'auth/oauth_callback')
        for b in base_urls
    ]
    return callback_urls


def compute_textract_app_url(args):
    if not args or not args.depl:
        return
    base_url = args.depl.rstrip('/')
    return f'{base_url}/apps/textract'


def create_client(args):
    if not (args.site and args.email and args.pw):
        print('site, email, pw are must to register a client. exiting')
        sys.exit()
    session = requests.session()
    session.post(
        os.path.join(args.site, 'accounts/v1/oauth/signin'),
        data={"email": args.email, "password": args.pw}
    ).raise_for_status()

    client_resp = session.post(os.path.join(args.site, 'accounts/v1/oauth/clients'), data={
        "name": "kaveri",
        "grant_types": json.dumps(["authorization_code", "refresh_token"]),
        "redirect_uris": json.dumps(compute_api_oauth_callback_uris(args)),
        "client_type": "private",
    })
    client_resp.raise_for_status()
    client_json  = client_resp.json()

    return {
        "client_id": client_json['client_id'],
        "client_secret": client_json['client_secret']
    }


def gen_site_config(args):
    if os.path.exists('site.json') and not args.reset:
        return
    if not (args.site_proxy or args.site):
        print('site url is required to setup kaveri. exiting')
        sys.exit()

    site = {
        "url": args.site_proxy or args.site
    }
    open('site.json', 'wb').write(
        json.dumps(site, indent=2, ensure_ascii=True).encode('utf-8')
    )


def gen_kconfig(args):
    if os.path.exists('kconfig.json') and not args.reset:
        return
    session = {
        "secret": secrets.token_hex(16)
    }
    apps = {
        "trusted_origins": [],
        "trusted_hostnames": ["_"]
    }
    client = create_client(args)

    kconfig = {
        "session": session,
        "apps": apps,
        "oauth": client
    }
    open('kconfig.json', 'wb').write(
        json.dumps(kconfig, indent=2, ensure_ascii=True).encode('utf-8')
    )


def gen_def_apps_config(args):
    if os.path.exists('default_apps.json') and not args.reset:
        return
    conf_item = {
        "name": "textract",
        "url": compute_textract_app_url(args)
    }
    conf = {}
    for c in ['ScannedBook', 'ScannedPage', 'ImageRegion', 'TextAnnotation']:
        conf[c] = conf_item.copy()
    open('default_apps.json', 'wb').write(
        json.dumps(conf, indent=2, ensure_ascii=True).encode('utf-8')
    )


def gen_config(args):
    gen_site_config(args)
    gen_def_apps_config(args)
    gen_kconfig(args)


def invoke_docker_compose(args):
    os.system("docker-compose up -d --build")


def main(argv):
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '-p', help='port', dest='port', required=False, default='9501'
    )
    parser.add_argument(
        '-r', help='reset', dest='reset', action='store_true'
    )
    parser.add_argument(
        '-m', '--mount_path', help='mount_path',
        dest='mount_path', default='', required=False
    )
    parser.add_argument(
        '-n', '--app_name', help='app_name',
        dest='app_name', default=default_app_name, required=False
    )
    parser.add_argument(
        '-V', '--version', help='version/branch/commit',
        dest='version', default=default_version, required=False
    )
    parser.add_argument(
        '-l', '--rebuild', help='Force rebuild of Docker container',
        dest='rebuild', action="store_true", required=False
    )
    parser.add_argument(
        '--site', help='site url', dest='site', required=False
    )
    parser.add_argument(
        '--site_proxy', help='site proxy url', dest='site_proxy', required=False, default=None
    )
    parser.add_argument(
        '--hostname', help='host name', dest='hostname', required=False, default=None
    )
    parser.add_argument(
        '--email', help='email', dest='email', required=False
    )
    parser.add_argument(
        '--pw', help='password', dest='pw', required=False
    )
    parser.add_argument(
        '--depl', help='deployment urls', dest='depl', required=False
    )
    parser.add_argument(
        '--vv_proxy', help='docker proxy common network', dest='vv_proxy', required=False, default='vv_proxy'
    )
    parser.add_argument(
        '--def-apps', help="default applications", dest='def_apps', action='store_true'
    )

    args, unknown = parser.parse_known_args()
    args.site = str(args.site).rstrip('/') if args.site else args.site
    #  args.mount_path = '/{}'.format(args.mount_path.lstrip('/').rstrip('/')).replace('//', '')
    args.mount_path = args.mount_path.lstrip('/')

    persist_env_vars(args)
    gen_config(args)
    invoke_docker_compose(args)

if __name__ == '__main__':
    main(sys.argv[:])
