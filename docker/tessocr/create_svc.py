import argparse
import sys
import os
import time

default_app_name = 'tessocr'
default_version = 'master'
default_vv_host_dir = '/opt/tessocr/'

def persist_env_vars(args):
    env_file_content = ''

    app_name = getattr(args, 'app_name', None)
    if app_name:
        env_file_content += "COMPOSE_PROJECT_NAME={}\n".format(app_name)

    env_file_content += "version={}\n".format(args.version)
    env_file_content += "port={}\n".format(args.port)
    env_file_content += "url_mount_path={}\n".format(args.url_mount_path)
    env_file_content += "rebuild_flag={}\n".format(time.time() if args.rebuild else 0)
    env_file_content += "vv_proxy={}\n".format(args.vv_proxy)
    open('.env', 'wb').write(env_file_content.encode('utf-8'))


def invoke_docker_compose():
    os.system("docker-compose up -d --build")


def main(argv):
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '-p', help='port', dest='port', required=False, default='9501'
    )
    parser.add_argument(
        '-n', '--app_name', help='app_name',
        dest='app_name', default=default_app_name, required=False
    )
    parser.add_argument(
        '-V', '--version', help='version/branch/coomit',
        dest='version', default=default_version, required=False
    )
    parser.add_argument(
        '-m', help='url mount path', dest='url_mount_path', required=False, default=''
    )
    parser.add_argument(
        '-r', help='reset', dest='reset', required=False, action='store_true'
    )
    parser.add_argument(
        '-l', '--rebuild', help='Force rebuild of Docker container',
        dest='rebuild', action="store_true", required=False
    )
    parser.add_argument(
        '--vv_proxy', help='docker proxy common network', dest='vv_proxy', required=False, default='vv_proxy'
    )

    args, unknown = parser.parse_known_args()

    persist_env_vars(args)
    invoke_docker_compose()


if __name__ == '__main__':
    main(sys.argv[:])
