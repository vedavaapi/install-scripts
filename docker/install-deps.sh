#!/usr/bin/env bash
#sudo apt-get -yq install software-properties-common
#sudo apt-add-repository universe
#sudo apt-get -yq  update
sudo apt -yq install python3-requests

dockerpath=`which docker`
dockercompose_path=`which docker-compose`
if [ -x "$dockerpath" ]; then
    echo "Found $dockerpath"
else
    sudo apt-get -yq install curl
    echo "Installing docker ..."
    curl -fsSL https://get.docker.com -o /tmp/get-docker.sh
    sudo sh /tmp/get-docker.sh
    sudo usermod -aG docker $USER
fi

if [ -x "$dockercompose_path" ]; then
    echo "Found $dockercompose_path"
else
    echo "Installing docker-compose ..."
    sudo curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
    newgrp docker
fi
