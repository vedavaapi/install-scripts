#!/usr/bin/env bash
version=$1
echo "Deploying Software Version ${version}"
declare repo_names=("apps/vv-console-ui" "apps/vv-console-api")
for repo in "${repo_names[@]}"; do
	giturl="https://gitlab.com/vedavaapi/${repo}.git"
	git clone ${giturl}
	cd $(echo "${repo}" | sed 's#^.*/##;') || exit
	git checkout ${version}
	cd - || exit
done
