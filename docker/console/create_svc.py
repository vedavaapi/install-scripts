import argparse
import json
import re
import subprocess
import sys
import os
import time

import requests

default_app_name = 'console'
default_version = 'master'
default_vv_creds_path = os.path.abspath('default_vv_creds.json')

def persist_env_vars(args):
    env_file_content = ''

    rebuild_ts = 0
    if os.path.exists(".env"):
        with open(".env") as f:
            lines = f.read().split('\n')
            for l in lines:
                if '=' not in l:
                    continue
                k,v = l.split('=')
                if k == 'rebuild_flag':
                    rebuild_ts = v
                    print("Retrieved ol  d rebuild_ts {}".format(rebuild_ts))
                    break

    app_name = getattr(args, 'app_name', None)
    if app_name:
        env_file_content += "COMPOSE_PROJECT_NAME={}\n".format(app_name)

    env_file_content += "mount_path={}\n".format(args.mount_path)
    env_file_content += "version={}\n".format(args.version)
    env_file_content += "port={}\n".format(args.port)
    # env_file_content += "distdir={}\n".format(args.distdir)
    env_file_content += "rebuild_flag={}\n".format(time.time() if args.rebuild else rebuild_ts)
    env_file_content += "vv_proxy={}\n".format(args.vv_proxy)

    open('.env', 'wb').write(env_file_content.encode('utf-8'))


def setup_volume_dirs(args):
    if args.reset:
         os.system('rm -rf {}'.format(args.distdir))
    if not os.path.exists(args.distdir):
        os.system('mkdir -p {}'.format(args.distdir))


def compute_api_oauth_callback_uris(args):
    base_urls = ["http://localhost:{}".format(args.port)]
    if(args.hostname):
        base_urls.append("http://{}:{}".format(args.hostname, args.port))
    ip_addr = re.split(
        '\s', subprocess.check_output(['hostname', '--all-ip-addresses']).decode('utf-8'))[0].strip()
    base_urls.append('http://{}:{}'.format(ip_addr, args.port))
    if args.depl:
        base_urls.append(args.depl.rstrip('/'))

    callback_urls = [
        os.path.join(b, args.mount_path.lstrip('/'), 'api/oauth/oauth_callback')
        for b in base_urls
    ]
    return callback_urls


def create_client(args):
    if os.path.exists('site_config.json') and not args.reset:
        return
    if not (args.site and args.email and args.pw):
        print('site, email, pw are must to register a client. exiting')
        sys.exit()
    session = requests.session()
    session.post(
        os.path.join(args.site, 'accounts/v1/oauth/signin'),
        data={"email": args.email, "password": args.pw}
    ).raise_for_status()

    client_resp = session.post(os.path.join(args.site, 'accounts/v1/oauth/clients'), data={
        "name": "console",
        "grant_types": json.dumps(["authorization_code", "refresh_token"]),
        "redirect_uris": json.dumps(compute_api_oauth_callback_uris(args)),
        "client_type": "private",
    })
    client_resp.raise_for_status()
    client_json  = client_resp.json()

    site_config = {
        "site_url": args.site_proxy or args.site,
        "client_id": client_json['client_id'],
        "client_secret": client_json['client_secret']
    }
    open('site_config.json', 'wb').write(
        json.dumps(site_config, indent=2, ensure_ascii=True).encode('utf-8'))


def setup_creds(args):
    os.system('cp "{}" "{}"'.format(os.path.abspath(args.vvcreds), 'vvcreds.json'))


def invoke_docker_compose(args):
    os.system("docker-compose up -d --build")


def main(argv):
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '-p', help='port', dest='port', required=False, default='9501'
    )
    parser.add_argument(
        '-r', help='reset', dest='reset', action='store_true'
    )
    parser.add_argument(
        '-m', '--mount_path', help='mount_path',
        dest='mount_path', default='', required=False
    )
    parser.add_argument(
        '-n', '--app_name', help='app_name',
        dest='app_name', default=default_app_name, required=False
    )
    parser.add_argument(
        '-V', '--version', help='version/branch/commit',
        dest='version', default=default_version, required=False
    )
    parser.add_argument(
        '-l', '--rebuild', help='Force rebuild of Docker container',
        dest='rebuild', action="store_true", required=False
    )
    parser.add_argument(
        '--site', help='site url', dest='site', required=False
    )
    parser.add_argument(
        '--site_proxy', help='site proxy url', dest='site_proxy', required=False, default=None
    )
    parser.add_argument(
        '--hostname', help='host name', dest='hostname', required=False, default=None
    )
    parser.add_argument(
        '--email', help='email', dest='email', required=False
    )
    parser.add_argument(
        '--pw', help='password', dest='pw', required=False
    )
    parser.add_argument(
        '--depl', help='deployment urls', dest='depl', required=False
    )
    parser.add_argument(
        '--vv_proxy', help='docker proxy common network', dest='vv_proxy', required=False, default='vv_proxy'
    )

    args, unknown = parser.parse_known_args()
    args.site = str(args.site).rstrip('/') if args.site else args.site
    #  args.mount_path = '/{}'.format(args.mount_path.lstrip('/').rstrip('/')).replace('//', '')
    args.mount_path = args.mount_path.lstrip('/')

    persist_env_vars(args)
    create_client(args)
    invoke_docker_compose(args)

if __name__ == '__main__':
    main(sys.argv[:])
