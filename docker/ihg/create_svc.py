import argparse
import json
import re
import sys
import time
from shutil import copyfile
# noinspection PyCompatibility

import os

default_site_name = 'myvvsite_ihg'
default_ihg_host_dir = '/opt/vedavaapi'
ihg_version = 'master'

def generate_conf_data(args):
    if not os.path.exists('conf_data'):
        os.makedirs('conf_data')

    org_config = {
    }

    orgs_config = {
        "default": org_config
    }

    with open('conf_data/orgs.json', 'wb') as orgs_file:
        orgs_file.write(json.dumps(orgs_config, indent=2, ensure_ascii=False).encode('utf-8'))


def setup_volume_dirs(host_dir, reset):
    for path in ('logs', ):
        path = os.path.join(host_dir, path)
        if reset:
            print("Removing {}".format(path))
            os.system('rm -rf {}'.format(path))
        if not os.path.exists(path):
            os.system('mkdir -p {}'.format(path))

def persist_env_vars(args):
    env_file_content = ''

    ihg_site_name = getattr(args, 'ihg_site_name', None)
    if ihg_site_name:
        env_file_content += "COMPOSE_PROJECT_NAME={}\n".format(ihg_site_name)
    env_file_content += "services={}\n".format(' '.join(args.services.split(',')))
    env_file_content += "url_mount_path={}\n".format(args.url_mount_path)
    env_file_content += "ihg_version={}\n".format(args.ihg_version)
    env_file_content += "ihg_log_dir={}\n".format(os.path.join(args.ihg_host_dir, 'logs'))
    env_file_content += "ihg_port={}\n".format(args.ihg_port)
    env_file_content += "books_data_dir={}\n".format(args.books)
    env_file_content += "rebuild_flag={}\n".format(time.time() if args.rebuild else 0)

    open('.env', 'wb').write(env_file_content.encode('utf-8'))


def invoke_docker_compose():
    print(os.getcwd())
    os.system("docker-compose up -d --build")


def main(argv):
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '-s', '--svc', help='comma-separated list of services to be launched', dest='services', required=False, default="iiif_image,iiif_presentation,ihg"
    )
    parser.add_argument(
        '-m', help='url mount path', dest='url_mount_path', required=False, default=''
    )
    parser.add_argument(
        '-d', help='vedavaapi host dir prefix', dest='ihg_host_dir', required=False, default=default_ihg_host_dir
    )
    parser.add_argument(
        '-r', '--reset', help='reset previous service', action='store_true', dest='reset'
    )
    parser.add_argument(
        '-p', "--port", help='port', dest='ihg_port', required=False, default='9010'
    )
    parser.add_argument(
        '-n', '--name', help='Vedavaapi site name',
        dest='ihg_site_name', default=default_site_name, required=False
    )
    parser.add_argument(
        '-V', '--version', help='Vedavaapi Software Version tag',
        dest='ihg_version', default=ihg_version, required=False
    )
    parser.add_argument(
        '-l', '--rebuild', help='force reload', dest='rebuild', required=False, action='store_true'
    )
    parser.add_argument(
        '--books', help='books data dir', dest='books', required=True
    )

    args, unknown = parser.parse_known_args()

    print('1. generating configuration data')
    generate_conf_data(args)
    print("2. creating vedavaapi volumes")
    setup_volume_dirs(args.ihg_host_dir, args.reset)
    print("3. persisting docker-compose env vars")
    persist_env_vars(args)
    print("4. invoking docker-compose......")
    invoke_docker_compose()


if __name__ == '__main__':
    main(sys.argv[:])
