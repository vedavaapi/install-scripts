version=$1
echo "Deploying Vedavaapi Software Version ${version}"
declare repo_names=("platform/core_services" "platform/vedavaapi_api" "platform/libvvobjstore" "platform/iiif" "libs/vv-client-python" "platform/ihg")
for repo in "${repo_names[@]}"; do
	giturl="https://gitlab.com/vedavaapi/${repo}.git"
	git clone ${giturl}
	cd $(echo "${repo}" | sed 's#^.*/##;')
	git checkout ${version}
    git pull
	#echo "export PYTHONPATH=\"\${PYTHONPATH}:$(pwd)\"" >> ~/.bashrc
	cd -
done
