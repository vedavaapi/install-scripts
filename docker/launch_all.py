import os
import sys
import argparse
import subprocess


def mycheck_output(cmd):
    try:
        # shellswitch = isinstance(cmd, collections.Sequence)
        # print "cmd:",cmd
        # print "type:",shellswitch
        shellval = False if (type(cmd) == type([])) else True
        return subprocess.Popen(cmd, shell=shellval, \
                                stderr=subprocess.PIPE, \
                                stdout=subprocess.PIPE).communicate()[0]
    except Exception as e:
        print("Error in ", cmd, ": ", e)
        return "error"


def parms2dict(str):
    components = str.split()
    cdict = {}
    i = 0
    while (i < len(components)):
        if components[i].startswith('-'):
            if i + 1 < len(components) and not components[i + 1].startswith('-'):
                cdict[components[i]] = components[i + 1]
                i = i + 1
            else:
                cdict[components[i]] = True
        i = i + 1
    # print cdict
    return cdict


def get_launch_parms(svc, port_base, rel, args):
    confs = {
        "vvsite": {
            "parms": ("-p {port} -m /{proj}/api" +
                      " -d {root}/{proj}/containers/vols/{svc} " +
                      " --gcreds {root}/vedavaapi_google_creds.json")
                .format(root=args.rootdir, svc="vvsite", port=port_base + 1, proj=rel)
        },
        "vvconsole": {
            "parms": ("-p {port} -m /{proj}/ui" +
                      " -d {root}/{proj}/containers/vols/{svc}")
                .format(root=args.rootdir, svc="vvconsole", port=port_base + 2,
                        proj=rel)
        },
        "console": {
            "parms": ("-p {port} -m /{proj}/console" +
                      " -d {root}/{proj}/containers/vols/{svc}")
                .format(root=args.rootdir, svc="console", port=port_base + 3,
                        proj=rel)
        },
        "segmenter": {
            "parms": ("-p {port} -m /{proj}/apps/segmenter" +
                      " -d {root}/{proj}/containers/vols/{svc}")
                .format(root=args.rootdir, svc="segmenter", port=port_base + 501,
                        proj=rel)
        },
        "mirador": {
            "parms": ("-p {port} -m /{proj}/apps/mirador" +
                      " -d {root}/{proj}/containers/vols/{svc}")
                .format(root=args.rootdir, svc="mirador", port=port_base + 502,
                        proj=rel)
        },
        "layout_detector": {
            "parms": ("-p {port} -m /{proj}/apps/ldetector" +
                      " -d {root}/{proj}/containers/vols/{svc}")
                .format(root=args.rootdir, svc="ldetector", port=port_base + 504,
                        proj=rel)
        },
        "tessocr": {
            "parms": ("-p {port} -m /{proj}/apps/tessocr" +
                      " -d {root}/{proj}/containers/vols/{svc}")
                .format(root=args.rootdir, svc="tessocr", port=port_base + 505,
                        proj=rel)
        },
        "ocreditor": {
            "parms": ("-p {port} -m /{proj}/apps/ocreditor" +
                      " -d {root}/{proj}/containers/vols/{svc}")
                .format(root=args.rootdir, svc="hocr_editor", port=port_base + 506,
                        proj=rel)
        },
        "googleocr": {
            "parms": ("-p {port} -m /{proj}/apps/googleocr" +
                      " -d {root}/{proj}/containers/vols/{svc}")
                .format(root=args.rootdir, svc="googleocr", port=port_base + 507,
                        proj=rel)
        },
        "ihg": {
            "parms": ("-p {port} -m /{proj}/apps/ihg" +
                      " -d {root}/{proj}/containers/vols/{svc}")
                .format(root=args.rootdir, svc="ihg", port=port_base + 508,
                        proj=rel)
        },
        "textdocs": {
            "parms": ("-p {port} -m /{proj}/apps/textdocs" +
                      " -d {root}/{proj}/containers/vols/{svc}")
                .format(root=args.rootdir, svc="textdocs", port=port_base + 509,
                        proj=rel)
        },
        "proxy": {
            "parms": ("-p {port}" +
                      " -d {root}/{proj}/containers/vols/{svc}")
                .format(root=args.rootdir, svc="proxy", port=port_base + 510,
                        proj=rel)
        }
    }

    if svc not in confs:
        return None
    parms = confs[svc]['parms']
    if rel:
        parms += " -n {}1{}1{}".format(args.orgprefix, rel, svc)
    if args.rebuild:
        parms += " -l"
    if args.version:
        parms += " -V {}".format(args.version)
    if args.reset:
        parms += " -r"
    return parms


def gen_conf(parms, ssl=True, server='apache2', for_vv_proxy=False):
    if server == 'apache2':
        st = """
            <Location {mnt}>
              ProxyPass http://{hostname}:{port}{mnt}
              ProxyPassReverse http://localhost:{port}{mnt}
              ProxyPreserveHost On
              RequestHeader set X-Forwarded-Port {sslport}
              RequestHeader set X-Forwarded-Scheme {proto}
            </Location>
    """
    else:
        st = """
            location {mnt} {{
                proxy_pass http://{hostname}:{port};
                # proxy_redirect     off;
                proxy_set_header X-Forward-Proto $scheme;
                proxy_set_header Host $host;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header X-Forwarded-Host $host:$server_port;
                proxy_set_header X-Forwarded-Port $server_port;
            }}
        """
    s = st.format(port=parms["-p"] if not for_vv_proxy else '80', mnt=parms.get("-m", ''),
           sslport=443 if ssl else 80,
           proto="https" if ssl else "http", hostname='localhost')
    return s


base_portnums = {
    "dev": 6000,
    "stage": 7000,
    "prod": 9000
}

default_root_dir = "/data/vedavaapi"
default_services = "vvsite,vvconsole,segmenter,mirador,layout_detector,tessocr,ocreditor,googleocr,ihg,textdocs,console,proxy"
default_releases = "prod"
default_version = "master"
default_orgprefix = "vv"


def unicode_for(astring, encoding='utf-8', ensure=False):
    # whether it is py2.7 or py3, or obj is str or unicode or bytes, this method will return unicode string.
    if isinstance(astring, bytes):
        return astring.decode(encoding)
    else:
        if ensure:
            return astring.encode(encoding).decode(encoding)
        else:
            return astring


def main(argv):
    global default_version
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '-s', '--svc', help='comma-separated list of services to be launched',
        dest='services', required=False, default=default_services
    )
    parser.add_argument(
        '--rel', help='comma-separated list of releases to be launched',
        dest='releases', required=False, default=default_releases
    )
    parser.add_argument(
        '--org', help='shortname of vedavaapi deployment instance',
        dest='orgprefix', required=False, default=default_orgprefix
    )
    parser.add_argument(
        '-d', help='install root dir', dest='rootdir', required=False, default=default_root_dir
    )
    parser.add_argument(
        '-r', '--reset', help='reset previous service', action='store_true', dest='reset'
    )
    parser.add_argument(
        '-V', '--version', help='Vedavaapi Software Version tag',
        dest='version', default=default_version, required=False
    )
    parser.add_argument(
        '-l', '--rebuild', help='force reload', dest='rebuild', required=False, action='store_true'
    )
    parser.add_argument(
        '-x', '--exit', help='shutdown all', dest='shutdown', required=False, action='store_true'
    )
    parser.add_argument(
        '-c', '--conf', help='generate server conf file', dest='conf', required=False, default=None
    )

    # parser.add_argument('--email', help='admin email if want to setup', dest='email', required=False, default=None)
    # parser.add_argument('--pw', help='admin password', dest='password', required=False, default=None)
    # parser.add_argument('--domain', help='domain name', dest='domain', required=False, default=None)

    args, unknown = parser.parse_known_args(argv)
    default_version = args.version

    mydir = os.path.dirname(os.path.abspath(__file__))

    print("Invoked from {}".format(mydir))
    if args.conf:
        os.system("mkdir -p {r}/{server}/http {r}/{server}/https".format(r=args.rootdir, server=args.conf))

        os.system("mkdir -p {r}/{server}/vvproxy/http {r}/{server}/vvproxy/https".format(r=args.rootdir, server=args.conf))

        #  os.system("rm -f {}/apache2/vv_services_*.conf".format(args.rootdir))
        print("Generating apache configurations in {}".format(args.rootdir))
    elif not args.shutdown:
        print("Launching in {}".format(args.rootdir))
        print("  (Re)installing docker on host")
        os.system("bash {}/install-deps.sh".format(mydir))

    report = {}

    for rel in args.releases.split(','):
        https_conf_file = "{}/{}/https/vv_services_{}.conf".format(args.rootdir, args.conf, rel)
        http_conf_file = "{}/{}/http/vv_services_{}.conf".format(args.rootdir, args.conf, rel)


        if args.conf:
            open(https_conf_file, 'w').write('')
            open(http_conf_file, 'w').write('')

        report[rel] = {}
        rundir = "{}/{}/containers/run".format(args.rootdir, rel)
        os.chdir(mydir)
        os.system("mkdir -p {}".format(rundir))
        os.system("cp -a *.sh *.py {}".format(rundir))
        os.chdir(rundir)
        print("Processing service {} in {}".format(rel, rundir))
        for svc_version in args.services.split(','):
            vals = svc_version.split(':')
            s = vals[0]
            v = vals[1] if len(vals) > 1 else None
            if args.shutdown:
                for f in unicode_for(mycheck_output("find {} -name .env -print".format(s).split())).split("\n"):
                    if not f:
                        continue
                    print(f)
                    dockerdir = os.path.dirname(os.path.abspath(f))
                    print("shutting down container(s) in ", dockerdir)
                    os.system("cd {}; docker-compose down -v ; rm -rf {}".format(dockerdir, f))
                continue

            args.version = v or default_version
            report[rel][s] = {"version": args.version}
            parms = get_launch_parms(s, base_portnums[rel], rel, args)
            parmsdict = parms2dict(parms)
            report[rel][s].update({
                "port": parmsdict['-p'],
                "mount_path": parmsdict.get('-m')
            })

            if args.conf:
                if s not in 'proxy':
                    print("Generating {} apache config for {}".format(rel, s))
                    with open(https_conf_file, "ab") as f:
                        f.write(gen_conf(parmsdict, ssl=True, server=args.conf).encode('utf-8'))
                    with open(http_conf_file, "ab") as f:
                        f.write(gen_conf(parmsdict, ssl=False, server=args.conf).encode('utf-8'))
            else:
                os.system("cp -a {mydir}/{svc} ./".format(mydir=mydir, svc=s))
                cmd = "(cd {svc}; python3 create_svc.py {parms} {extras})".format(rel=rel, svc=s, parms=parms,
                                                                       extras=' '.join(unknown))
                print(cmd)
                os.system(cmd)

    from subprocess import check_output
    import re
    ip_addr = re.split('\s',  unicode_for(check_output(['hostname', '--all-ip-addresses'])))[0].strip()
    for rel in report:
        print('\n\n\n', rel, ':\n================')
        for s, conf in report[rel].items():
            print('{}: http://{}:{}/{}'.format(s, ip_addr, conf['port'], conf['mount_path']))

    return report

if __name__ == '__main__':
    main(sys.argv[:])
