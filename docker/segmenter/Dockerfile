FROM ubuntu:latest
ARG DEBIAN_FRONTEND=noninteractive

# first install python3, pip3, mongodb, apache
RUN apt-get -yq update
RUN apt-get -yq install nano git python3 python3-pip
RUN apt-get install -yq apt-utils vim curl apache2 apache2-utils libexpat1 ssl-cert libapache2-mod-wsgi-py3
RUN a2enmod wsgi

# install dependencies
RUN apt-get -yq install python3-requests; \
	apt-get  -yq install libsm6 libxext6 libxrender-dev ; \
	apt-get -yq install python3-numpy python3-scipy python3-matplotlib python3-opencv; \
	pip3 install scikit-image; \
	apt-get -yq install python3-pil python3-flask; \
	pip3 install flask-cors flask-restplus;

RUN apt-get -yq install python3-keras python3-sklearn python3-ipython
RUN pip3 install tensorflow
RUN pip3 install -U "celery[redis]"

# now clone vedavaapi repositories
ENV SRC_DIR "/opt/word-segmenter/src"
RUN mkdir -p $SRC_DIR;

RUN chown www-data ${SRC_DIR}
USER www-data

WORKDIR $SRC_DIR

# get git repos
ARG rebuild_flag
ARG version
RUN echo "imanal version -:- ${version}" > version-info.txt
ARG vedavaapi_repos_clone_script="clone_vedavaapi_repos.sh"
COPY ${vedavaapi_repos_clone_script} ./
RUN bash ${vedavaapi_repos_clone_script} ${version}

ENV PYTHONPATH "${PYTHONPATH}:${SRC_DIR}/word-segmenter:${SRC_DIR}/vv-client-python:${SRC_DIR}/lib-wsegmenter:${SRC_DIR}/libvv-textract-segmenters-common"

# symlink apache-conf
USER root
ARG sites_available_dir="/etc/apache2/sites-available"
ARG sa_http_conf_link_path="${sites_available_dir}/word-segmenter.conf"
COPY apache_conf_template.conf "${sa_http_conf_link_path}"
RUN sed -i "s#{IMANAL_DIR}#${SRC_DIR}/word-segmenter#g;" "${sa_http_conf_link_path}"
ARG url_mount_path
RUN sed -i "s#{MOUNT_PATH}#${url_mount_path}#g;" "${sa_http_conf_link_path}"

ARG sites_enabled_dir="/etc/apache2/sites-enabled"
RUN ln -s ${sa_http_conf_link_path} "${sites_enabled_dir}/word-segmenter.conf";

ENV APACHE_RUN_DIR /var/run/apache2
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2
RUN apache2ctl start

EXPOSE 80
